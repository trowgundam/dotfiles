local wezterm = require("wezterm")
local hostname = wezterm.hostname()
local mux = wezterm.mux

local config = {}
local launch_menu = {}

if wezterm.config_builder then
	config = wezterm.config_builder()
end

if wezterm.target_triple == "x86_64-pc-windows-msvc" then
	config.default_prog = { "pwsh.exe", "-NoLogo" }

	table.insert(launch_menu, {
		label = "PowerShell",
		args = { "powershell.exe", "-NoLogo" },
	})
	table.insert(launch_menu, {
		label = "PwSh",
		args = { "pwsh.exe", "-NoLogo" },
	})

	wezterm.on("gui-startup", function()
		local _, _, window = mux.spawn_window({})
		window:gui_window():maximize()
	end)

	config.win32_system_backdrop = "Tabbed"
end

config.color_scheme = "Catppuccin Mocha"
config.font = wezterm.font("FiraCode Nerd Font")
config.launch_menu = launch_menu
config.hide_tab_bar_if_only_one_tab = true
config.use_fancy_tab_bar = false
config.window_background_opacity = 0.7
config.initial_cols = 120

if string.lower(hostname) == "mali-dei" then
	config.font_size = 10
	config.max_fps = 120
elseif string.lower(hostname) == "mali-beni" then
	config.font_size = 10
	config.max_fps = 144
end

local mouse_bindings = {
	{
		event = { Down = { streak = 3, button = "Left" } },
		action = wezterm.action.SelectTextAtMouseCursor("SemanticZone"),
		mods = "NONE",
	},
}
config.mouse_bindings = mouse_bindings

config.ssh_domains = {
	{
		name = "mali-dei",
		remote_address = "172.16.3.1",
		username = "celeb",
		multiplexing = "None",
		assume_shell = "Unknown",
		remote_wezterm_path = "C:\\Program Files\\WezTerm\\wezterm.exe",
	},
	{
		name = "mali-beni",
		remote_address = "172.16.3.2",
		username = "jeff",
		multiplexing = "WezTerm",
		assume_shell = "Posix",
	},
	{
		name = "Unraid",
		remote_address = "172.16.2.1",
		username = "root",
		multiplexing = "None",
		assume_shell = "Posix",
	},
}

return config
