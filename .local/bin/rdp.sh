#!/bin/bash

read -r -s -p "Password: " password
#echo
#echo $password

if [[ "$XDG_SESSION_TYPE" == "wayland" ]]; then
	sdl-freerdp3 $@ /w:1920 /h:1080 /d:insequence.local /u:jeffgeer "/p:$password"
elif [[ "$XDG_SESSION_TYPE" == "x11" ]]; then
	xfreerdp3 $@ /w:1920 /h:1080 /d:insequence.local /u:jeffgeer "/p:$password"
fi
