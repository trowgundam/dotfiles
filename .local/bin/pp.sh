#!/bin/bash

if [[ -f /tmp/old-pp.txt ]]; then
	powerprofilesctl set "$(/usr/bin/cat /tmp/old-pp.txt)"
	rm /tmp/old-pp.txt
else
	powerprofilesctl get >/tmp/old-pp.txt
	powerprofilesctl set "$1"
fi
