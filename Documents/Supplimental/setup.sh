#!/bin/bash

SYSPATH="$HOME/Documents/Supplimental/systems/$(uname -n)"
if [ -d $SYSPATH ]; then
	cp -a $SYSPATH/. $HOME
fi;

rustup install stable
opam init
/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME submodule update --init --recursive -j 5
cd $HOME/.config/nvim
git checkout main
cd $HOME/.config/hypr
git checkout main
cd $HOME/.config/qtile
git checkout main
cd $HOME/.config/zsh
git checkout master
ln -s $HOME/.config/zsh/home_zshenv $HOME/.zshenv
sudo cp -r $HOME/Documents/Supplimental/root/* /
sudo mkdir -m 777 /mnt/Unraid
sudo systemctl enable --now mnt-Unraid.automount
systemctl --user enable --now ssh-agent.service
sudo timedatectl set-ntp true
$HOME/.config/hypr/env.sh
$HOME/.config/hypr/systems/setup.sh
